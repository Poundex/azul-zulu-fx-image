
FROM ubuntu

ENV JAVA_HOME="/home/java/.sdkman/candidates/java/18.fx-zulu"
ENV PATH="$JAVA_HOME/bin:${PATH}"
RUN echo "PATH=\"$PATH\"" > /etc/environment

RUN apt-get update
RUN apt-get install --assume-yes --force-yes zip unzip curl
RUN apt-get autoremove --yes
RUN rm -rf /var/lib/{apt,dpkg,cache,log}
RUN useradd -m java

USER java
WORKDIR /home/java

RUN curl -o sdkman.sh  -s "https://get.sdkman.io" 
RUN chmod +x sdkman.sh
RUN ./sdkman.sh
RUN bash -i -c -- "sdk install java 18.fx-zulu"

COPY entrypoint.sh /home/java/entrypoint.sh
#ENTRYPOINT ["/home/java/entrypoint.sh"]
#CMD [""]
